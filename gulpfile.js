const gulp = require('gulp');
const mocha = require('gulp-mocha');
const eslint = require('gulp-eslint');
//const guppy = require('git-guppy')(gulp);

gulp.task('unit', () => {
	gulp.src('test/*.spec.js', { read: false })
		.pipe(mocha({ reporter: 'nyan' }))
		.once('error', () => {
			process.exit(1);
		});
});

gulp.task('pre-commit', ['lint', 'unit']);

// gulp.task('prepare-commit-msg', guppy.src('prepare-commit-msg', function () {
// 	// eslint-disable-next-line
// 	// console.log(arguments);
// 	// process.exit(1);
// })
// );

gulp.task('prepare-commit-msg');

gulp.task('lint', () => {
	return gulp.src(['**/*.js', '!node_modules/**'])
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});
